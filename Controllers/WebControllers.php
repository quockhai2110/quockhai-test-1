<?php  

	Class WebControllers {
		public function login() {
			require_once('Views/LoginViews.html');

		}

		public function register() {
			require_once('Views/RegisterViews.html');
		}


		public function handlelogin() {
			require_once('Models/WebModels.php');
			$username = $_POST['username'];
			$password = $_POST['password'];

			$person = new WebModels();
			if( !$person->LoginModels( $username, $password)){
				echo "<script>alert(\"Sai tên tài khoản hoặc mật khẩu!\")</script>";
				echo "<script>window.location.replace(\"http://localhost:2300/XayDungWebAnToan\")</script>";
			}else{
				require_once('Views/LoginSuccessViews.html');
			}
		}

		public function handleregister(){
			require_once('Models/WebModels.php');
			$username = $_POST['username'];
			$password = $_POST['password'];
			$passwordconfirm = $_POST['confirmpassword'];
			$email = $_POST['email'];
			$phone = $_POST['phone'];
			$address = $_POST['address'];

	
			if($password != $passwordconfirm){
				echo "<script>alert(\"Password không trùng nhau\")</script>";
				echo "<script>window.location.replace(\"http://localhost:2300/XayDungWebAnToan/user/register\")</script>";
			}

			$pattern_password = "/^\S*(?=\S{8,})(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\d])(?=\S*\W+)\S*$/";
			if(preg_match($pattern_password, $password) == 0){
				echo "<script>alert(\"Mật khẩu phải lớn hơn 8 kí tự, có ít nhất một chữ in hoa, ít nhất một chữ số, ít nhất một kí tự đặc biệt\")</script>";
				echo "<script>window.location.replace(\"http://localhost:2300/XayDungWebAnToan/user/register\")</script>";
			}

			$pattern_phone = "/[0-9]{10}/";

			if(preg_match($pattern_phone, $phone) == 0){
				echo "<script>alert(\"Số điện thoại không hợp lệ!\")</script>";
				echo "<script>window.location.replace(\"http://localhost:2300/XayDungWebAnToan/user/register\")</script>";
			}

			$pattern_email = "/[a-zA-Z0-9]{3,}@(.){3,}\.(.){3,}/";
			if(preg_match($pattern_email, $email) == 0){
				echo "<script>alert(\"Email không hợp lệ!\")</script>";
				echo "<script>window.location.replace(\"http://localhost:2300/XayDungWebAnToan/user/register\")</script>";
			}


			$person = new WebModels();
			$info = $person->RegisterModels( $username, $password, $email, $phone, $address);
			if($info){
				echo "<script>alert(\"$info\")</script>";
				echo "<script>window.location.replace(\"http://localhost:2300/XayDungWebAnToan/user/register\")</script>";
			}

			echo "Đăng kí thành công";
		}

			public function changepassword(){
				require_once('Views/changepasswordViews.html');

		}
	}
	
?>